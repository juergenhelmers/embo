require 'test_helper'

class PracticalModulesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:practical_modules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create practical_module" do
    assert_difference('PracticalModule.count') do
      post :create, :practical_module => { }
    end

    assert_redirected_to practical_module_path(assigns(:practical_module))
  end

  test "should show practical_module" do
    get :show, :id => practical_modules(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => practical_modules(:one).to_param
    assert_response :success
  end

  test "should update practical_module" do
    put :update, :id => practical_modules(:one).to_param, :practical_module => { }
    assert_redirected_to practical_module_path(assigns(:practical_module))
  end

  test "should destroy practical_module" do
    assert_difference('PracticalModule.count', -1) do
      delete :destroy, :id => practical_modules(:one).to_param
    end

    assert_redirected_to practical_modules_path
  end
end

class InstructorsController < ApplicationController
  layout 'course'
  #:allow_to, :check_user, :set_profile, 
  before_filter :admin_required, :except => [ :index, :show ]
  
  # GET /instructors
  # GET /instructors.xml
  def index
    @instructors = Instructor.find(:all, :order => 'last_name ASC')

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @instructors }
    end
  end

  # GET /instructors/1
  # GET /instructors/1.xml
  def show
    @instructor = Instructor.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @instructor }
    end
  end
end

class PracticalModulesController < ApplicationController
  layout 'course'
  before_filter :login_required
  
  # GET /practical_modules
  # GET /practical_modules.xml
  def index
    @practical_modules = PracticalModule.find(:all, :conditions => [ "course_year = ?", "#{APP_CONFIG['course_year']}"])

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @practical_modules }
    end
  end

  # GET /practical_modules/1
  # GET /practical_modules/1.xml
  def show
    @practical_module = PracticalModule.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @practical_module }
    end
  end

  
end

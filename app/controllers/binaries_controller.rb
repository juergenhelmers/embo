class BinariesController < ApplicationController
  layout 'course'
  #:allow_to, :check_user, :set_profile, 
  before_filter :login_required
  
  # GET /instructors
  # GET /instructors.xml
  def index
    @documents = Binary.published

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @instructors }
    end
  end

end

class CourseSignupsController < ApplicationController
  
  layout 'course'
  
  def index
    
  end
  
  def new
    
  end
  
  def show
    
  end
  
  def create
    @user = User.new(params[:user])
    
    if @user.errors.empty? && @user.save
      UserMailer.deliver_registration_confirmation(@user)
 #     redirect_back_or_default('/')
      flash.now[:notice] = "Thank you for your Registration."
      render :action => 'thank_you'
    else
       render :action => 'new'
    end
  end
end

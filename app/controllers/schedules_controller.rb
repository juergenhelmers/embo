class SchedulesController < ApplicationController
    layout 'course'
  def index
    
  end
  
  def show
    @schedule = Schedule.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @instructor }
    end
  end
end

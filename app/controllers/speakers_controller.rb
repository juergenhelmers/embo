class SpeakersController < ApplicationController
  layout 'course'
  # GET /speakers
  # GET /speakers.xml
  def index
    @speakers = Speaker.find(:all, :order => 'last_name ASC')

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @speakers }
    end
  end

  # GET /speakers/1
  # GET /speakers/1.xml
  def show
    @speaker = Speaker.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @speaker }
    end
  end

  
end

class Admin::InstructorsController < Admin::AdminController
  layout 'admin'
  uses_tiny_mce :options => {
                              :theme => 'advanced',
                              :theme_advanced_resizing => true,
                              :theme_advanced_resize_horizontal => false,
                              :theme_advanced_buttons1_add => "media",
                              :plugins => %w{ table fullscreen safari media},
                              :width => '640',
                              :theme_advanced_toolbar_align => "left"


                            }

  active_scaffold :instructor do |config|
    #labels
    config.list.label = "Course Instructors"
    config.create.label = "Add Instructor"
    config.update.label = "Update Instructor"
    config.show.label = "Instructor Details"
    
    config.columns[:body].form_ui = :text_editor    
    
    #sorting
    config.list.sorting = { :last_name => :ASC }
    
    
    #column definitions
    config.columns = [:first_name, :last_name, :organisation, :country, :webpage, :email]
    config.list.columns = [:first_name, :last_name, :organisation, :email]
    config.create.columns = [:first_name, :last_name, :email, :organisation]
    config.update.columns = [:first_name, :last_name, :email, :organisation]
    config.show.columns = [:first_name, :last_name, :email, :organisation]
    
    #column options
    config.columns[:body].options = {:rows => '30'}
   
    
  end
end

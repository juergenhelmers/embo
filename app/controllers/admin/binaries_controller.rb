class Admin::BinariesController < Admin::AdminController

  active_scaffold :binary do |config|
  
    # sorting
    config.list.sorting = { :document_file_name => :asc }
    
    #restrict all columns to these three
    config.columns = [:document, :comment, :published]
    config.list.columns = [:document_file_name, :comment,:published,:document_course_year]
    config.create.columns = [:document, :comment, :published]
    config.actions.exclude :edit
    config.actions.exclude :show
    
    config.columns[:document].form_ui = :paperclip
    config.columns[:published].form_ui = :checkbox
    

    #include multipart for create and update forms
    config.create.multipart = true
    config.update.multipart = true
    
    #labels
    config.list.label = "Document Files"
    config.create.label = "Add Document"
    config.update.label = "Update Document"
    
    
    config.columns[:document].label = "Document"
    config.columns[:document_file_name].label = "File Name"
    config.columns[:comment].label = "Description"
    config.columns[:published].label = "published"
    config.columns[:document_course_year].label = "Course Year" 
    
   
    
  end
  
  def before_create_save(record)
    record.user_id = @current_user.id
  end
  

end





class Admin::ParticipantsController < Admin::AdminController
  active_scaffold :participant do |config|
    #labels
    config.list.label = "External Particpants"
    config.create.label = "Add Participant"
    config.update.label = "Update Participant"
    config.show.label = "Participant Details"
    
    config.columns[:first_name].label = "First Name"
    config.columns[:last_name].label = "Last Name"
    
    # form_ui definitions
    
    config.columns[:birthday].form_ui = :calendar_date_select
    
    config.columns[:birthday].options = {:year_range => [60.years.ago, 18.years.ago]} 
    
    #sorting
    config.list.sorting = { :last_name => :ASC }
    
    config.list.columns=[:first_name, :last_name, :email]
    config.create.columns=[:login, :first_name, :last_name, 
                        :email, :birthday]
  end
  
  def after_create_save(record)
    UserMailer.deliver_registration_ext_confirmation(record)
  end

    
  
  
  # only authenticated admin users are authorized to create users
  def create_authorized?
   @current_user.role.name=="admin"
  end
  # only authenticated admin users are authorized to update users
  def update_authorized?
   @current_user.role.name=="admin"
  end
  # only authenticated admin users are authorized to delete users
  def delete_authorized?
   @current_user.role.name=="admin"  
 end
  # only authenticated admin users are authorized to delete users
  def show_authorized?
   @current_user.role.name=="admin" || @current_user.role.name=="organizer" 
  end
  
end

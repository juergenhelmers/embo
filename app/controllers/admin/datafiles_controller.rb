class Admin::DatafilesController < Admin::AdminController
  layout 'admin'
  active_scaffold :datafile do |config|
    
    #labels
    config.list.label = "Letters of Recommendation (Attached File)"
    config.create.label = "Add Letter"
    config.update.label = "Update Letter"
    config.show.label = "Letter Details"
    config.create.link.label = "Add File"
    # sorting
    config.list.sorting = { :letter_file_name => :asc }
    
    #restrict all columns to these three
    config.columns = [:letter,:comment]
    config.actions.exclude :show
    config.actions.exclude :edit
    
    config.columns[:letter].form_ui = :paperclip

    #include multipart for create and update forms
    config.create.multipart = true
    config.update.multipart = true
    
    #label II
    config.columns[:letter].label = "File Name"
    config.columns[:comment].label = "File Description"
        
  end
  
end

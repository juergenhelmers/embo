class Admin::PracticalModulesController < ApplicationController
  layout 'admin'
  
  active_scaffold :practical_modules do |config|
    
  config.list.label = "Practical Modules"
  config.update.label = "Update Practical Module"
  config.show.label = "Practical Module Details"
    
  config.columns[:group1_early].label = "G1 (early)"
  config.columns[:group2_early].label = "G2 (early)"
  config.columns[:group3_early].label = "G3 (early)"
  config.columns[:group4_early].label = "G4 (early)"
  config.columns[:group1_late].label = "G1 (late)"
  config.columns[:group2_late].label = "G2 (late)"
  config.columns[:group3_late].label = "G3 (late)"
  config.columns[:group4_late].label = "G4 (late)"
  
    
  config.list.sorting = { :course_date => :ASC }
  
  config.list.columns = [:course_date, :group1_early, :group1_late, :group2_early, :group2_late,:group3_early,:group3_late,:group4_early,:group4_late]
  config.create.columns = [:course_date, :group1_early, :group1_late, :group2_early, :group2_late,:group3_early,:group3_late,:group4_early,:group4_late]
  config.update.columns = [:course_date, :group1_early, :group1_late, :group2_early, :group2_late,:group3_early,:group3_late,:group4_early,:group4_late]
  config.show.columns = [:course_date, :group1_early, :group1_late, :group2_early, :group2_late,:group3_early,:group3_late,:group4_early,:group4_late]
  end
  
end

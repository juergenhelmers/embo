class Admin::SpeakersController < Admin::AdminController
  layout 'admin'
  uses_tiny_mce :options => {
                              :theme => 'advanced',
                              :theme_advanced_resizing => true,
                              :theme_advanced_resize_horizontal => false,
                              :theme_advanced_buttons1_add => "media",
                              :plugins => %w{ table fullscreen safari media},
                              :width => '640',
                              :theme_advanced_toolbar_align => "left"


                            }

  active_scaffold :speaker do |config|
    #labels
    config.list.label = "Course Speakers"
    config.create.label = "Add Speaker"
    config.update.label = "Update Speaker"
    config.show.label = "Speaker Details"
    config.columns[:title].label = "Title of Talk"
    config.columns[:first_name].label = "First Name"
    config.columns[:last_name].label = "Last Name"
    config.columns[:postal_code].label = "Postal Code"
    config.columns[:body].label = "Research Description"
    
    config.columns[:body].form_ui = :text_editor    
    
    #sorting
    config.list.sorting = { :last_name => :ASC }
    
    
    #column definitions
    config.columns = [:first_name, :last_name, :title, :body, :organisation, :address, 
                   :postal_code, :city, :country, :webpage, :email, :telephone]
    config.list.columns = [:first_name, :last_name, :organisation, :city, :email]
    config.create.columns = [:title, :first_name, :last_name, :email, :telephone, 
                         :organisation, :address, :postal_code, :city, :country, 
                         :webpage, :body ]
    config.update.columns = [:title, :first_name, :last_name, :email, :telephone, 
                         :organisation, :address, :postal_code, :city, :country, 
                         :webpage, :body ]
    config.show.columns = [:title, :first_name, :last_name, :email, :telephone, 
                         :organisation, :address, :postal_code, :city, :country, 
                         :webpage, :body ]
    
    #column options
    config.columns[:body].options = {:rows => '30'}
   
    
  end
end

class Admin::UsersController < Admin::AdminController
  active_scaffold :user do |config|
    
    config.actions.exclude :create
       
    #labels
    config.list.label = "System Users"
    config.update.label = "Update User"
    config.show.label = "User Details"
    config.columns[:login].label = "User Name"
    config.columns[:first_name].label = "First Name"
    config.columns[:last_name].label = "Last Name"
    config.columns[:email].label = "Email Address"
    config.columns[:active].label = "Accepted"
    config.columns[:datafiles].label = "Letter(s) of Recommendation"
    config.columns[:created_at].label = "Application Date"
    config.columns[:rating_average].label = "Rating"
    config.columns[:waved].label = "applied for wave"
    config.columns[:hear_about_the_course].label = "How did you hear about the course?"
    config.columns[:letter_of_recommendation_text].label = "1st Letter Text"
    config.columns[:letter_of_recommendation_text2].label = "2nd Letter Text"

    # form ui definitions
    config.columns[:active].form_ui = :checkbox
    #config.columns[:letter_of_recommendation_text].form_ui = :text_editor
    
    
    
    #sorting
    config.list.sorting = { :rating_average => :DESC }
    
    #column definitions
    config.columns = [:login, :admin, :first_name, :last_name, :email]
    config.list.columns = [:id, :first_name, :last_name, :email, :rating_average, :active]
    config.update.columns = [:id, :login, :active, :title, :first_name, :last_name, :email,
    :department, :organisation, :course_year, :role_id, :cv_file, :waved]
    config.show.columns = [:course_year, :created_at, :id, :login, :active, :title, :first_name, :last_name, :email,
    :birthday, :nationality, :department, :organisation, :address1, :address2, :postal_code, :city,
    :user_country, :telephone, :fax_number, :mobile,:current_position, :supervisor, 
    :reason_to_apply, :mic_exp, :languages, :science_exp, :awards, :publications,
    :background, :benefit, :hear_about_the_course, :waved, :cv_file, :letter_of_recommendation_text, :letter_of_recommendation_text2, :datafiles]
        
    config.update.columns.add_subgroup "Letters of Recomendation (Attachments)" do |namegroup|
      namegroup.add :datafiles
    end
    

  # force assignment of user_id when creating a new record
  def do_new
    super
    @user_session_id = 0
  end
  
  def after_create_save(record)
    save_user_id(record)
  end

  def before_create_save(record)
    record.datafiles.user_id = record.id
  end
  
 
    config.action_links.add "accept_user", :action=>"accept_user", 
        :method=>:put, :type=>:member, 
        :label=>"Accept", :crud_type=>:update, 
        :inline=>false
 
    config.action_links.add "reject_user", :action=>"reject_user", 
        :method=>:put, :type=>:member, 
        :label=>"Reject", :crud_type=>:update, 
        :inline=>false
    
  end
  
  # accept user function
  def accept_user
    @user = User.find(params[:id])
    if @user.active == false 
      # activate user account
      @user.active = 'true'
      # accept user for system checks
      @user.accepted = 'true'
      #set activated_at timestamp
      @user.activated_at = Time.now.utc
      #  update the user object, save it and send out Acceptance Email
      @user.attributes = params[:user]
      @user.save false
      UserMailer.deliver_acceptance_letter(@user)
      
    else
      flash[:error] = "The user is already accepted and activated!"
      redirect_to admin_users_path
     
    end
  end

  def conditions_for_collection
    { :course_year => APP_CONFIG['course_year'].to_s } 
  end
  
  #reject user function
  def reject_user
    @user = User.find(params[:id])
      # activate user account
      @user.active = 'false'
      # accept user for system checks
      @user.accepted = 'false'
      #set activated_at timestamp
      @user.activated_at = nil
      #  update the user object, save it and send out Acceptance Email
      @user.attributes = params[:user]
      @user.save false
      UserMailer.deliver_rejection_letter(@user)
    
  end
  
#  def rate
#    @user = User.find(params[:id])
#    @user.rate(params[:stars], current_user)
#    id = "ajaxful-rating-record-#{@user.id}"
#    render :update do |page|
#      page.replace_html id, ratings_for(@user, :wrap => false, :dimension => params[:dimension])
#      page.visual_effect :highlight, id
#  end
# end
  def rate
    @record = User.find(params[:id])
    @record.rate(params[:stars], current_user)
    
    render :update do |page|
      page.replace_html @record.wrapper_dom_id(params), ratings_for(@record, params.merge(:wrap => false))
      page.visual_effect :highlight, @record.wrapper_dom_id(params)
    end
  end

  # only authenticated admin users are authorized to accept users
#  def accept_user_authorized?
#   @current_user.role.name=="admin"
#  end
  # only authenticated admin users are authorized to update users
  def update_authorized?
   @current_user.role.name=="admin"
  end
  # only authenticated admin users are authorized to delete users
  def delete_authorized?
   @current_user.role.name=="admin"  
 end
  # only authenticated admin users are authorized to delete users
  def show_authorized?
   @current_user.role.name=="admin" || @current_user.role.name=="organizer" 
 end
 
#  def conditions_for_collection
#    "course_year = #{APP_CONFIG['course_year'].to_s} AND role_id='3'"
#  end
 
 
end

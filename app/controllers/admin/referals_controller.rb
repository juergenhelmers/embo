class Admin::ReferalsController < Admin::AdminController
  layout 'admin'
  
  active_scaffold :referal do |config|
     #labels
    config.list.label = "Course Referals"
    config.show.label = "Referal Details"
    config.columns[:you_name].label = "Sender's Name"
    config.columns[:you_email].label = "Sender's Email"
    config.columns[:name].label = "Recipient"
    config.columns[:email].label = "Recipient's Email"
    config.columns[:comment].label = "Sender's Comment"
    
    #limitations
    config.actions.exclude :create
    config.actions.exclude :update
    
    #sorting
    config.list.sorting = { :created_at => :ASC }
    
    #column definitions
    config.columns = [:you_name, :you_email, :name, :email, :comment]
    
  end
end

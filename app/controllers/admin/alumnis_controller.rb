class Admin::AlumnisController < Admin::AdminController
  active_scaffold :alumni do |config|
    
    config.actions.exclude :create
       
    #labels
    config.list.label = "System Users"
    config.update.label = "Update User"
    config.show.label = "User Details"
    config.columns[:login].label = "User Name"
    config.columns[:first_name].label = "First Name"
    config.columns[:last_name].label = "Last Name"
    config.columns[:email].label = "Email Address"
    config.columns[:active].label = "Accepted" 
    config.columns[:datafiles].label = "Letter(s) of Recommendation"
    config.columns[:letter_of_recommendation_text].label = "1st Letter of Recommendation Text"
    config.columns[:letter_of_recommendation_text2].label = "2nd Letter of Recommendation Text"
    config.columns[:created_at].label = "Application Date"
    
    # form ui definitions
    config.columns[:active].form_ui = :checkbox
    #config.columns[:letter_of_recommendation_text].form_ui = :text_editor
    
    
    
    
    
    #sorting
    config.list.sorting = { :id => :ASC }
    
    #column definitions
    config.columns = [:login, :admin, :first_name, :last_name, :email]
    config.list.columns = [:id, :first_name, :last_name, :email, :active]
    config.update.columns = [:id, :login, :password, :password_confirmation, :active, :title, :first_name, :last_name, :email,
    :department, :organisation, :course_year, :role_id, :letter_of_recommendation_text, :letter_of_recommendation_text2]
    config.show.columns = [:course_year, :created_at, :id, :login, :active, :title, :first_name, :last_name, :email,
    :birthday, :nationality, :department, :organisation, :address1, :address2, :postal_code, :city,
    :user_country, :telephone, :fax_number, :mobile,:current_position, :supervisor, 
    :learn_course, :reason_to_apply, :mic_exp, :languages, :science_exp, :awards, :publications,
    :background, :benefit, :cv_file, :letter_of_recommendation_text, :letter_of_recommendation_text2, :datafiles]


#    config.update.columns.add_subgroup "Letters of Recomendation (Attachments)" do |namegroup|
#      namegroup.add :datafiles
#      namegroup.collapsed = true
#    end
    

  # force assignment of user_id when creating a new record
  def do_new
    super
    @user_session_id = 0
  end
  
  def after_create_save(record)
    save_user_id(record)
  end

  def before_create_save(record)
    record.datafiles.user_id = record.id
  end
  

end
  # only authenticated admin users are authorized to accept users
#  def accept_user_authorized?
#   @current_user.role.name=="admin"
#  end
  # only authenticated admin users are authorized to update users
  def update_authorized?
   @current_user.role.name=="admin"
  end
  # only authenticated admin users are authorized to delete users
  def delete_authorized?
   @current_user.role.name=="admin"  
 end
  # only authenticated admin users are authorized to delete users
  def show_authorized?
   @current_user.role.name=="admin" || @current_user.role.name=="organizer" 
  end
end

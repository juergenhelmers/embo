class Admin::SchedulesController < ApplicationController
  layout 'admin'
  
  active_scaffold :schedule do |config|
    
  config.columns[:morning1].label = "09:00-10:00"
  config.columns[:morning2].label = "10:00-11:00"
  config.columns[:morning3].label = "11:00-12:30"
  config.columns[:morning1].description = "Morning 1"
  config.columns[:morning2].description = "Morning 2"
  config.columns[:morning3].description = "Morning 3"
  config.columns[:lunch].label = "12:30-13:30"
  config.columns[:afternoon1].label = "13:30-15:30"
  config.columns[:afternoon2].label = "16:00-19:15"
  config.columns[:afternoon1].description = "Practical Module I"
  config.columns[:afternoon1b].description = "Practical Module II"
  config.columns[:afternoon2].description = "Afternoon 2"
  config.columns[:dinner].label = "20:00-21:30"
  
  config.create.multipart = true
  config.update.multipart = true
  
  
  config.list.sorting = { :course_date => :ASC }
  
  config.list.columns = [:course_date, :all_day, :venue,  :morning1, :morning2,:morning3]
  config.create.columns = [:course_date, :all_day, :venue, :morning1, :filemorning1, :morning2, :filemorning2,:morning3, :filemorning3, :lunch,:afternoon1,:afternoon1b,:afternoon2,:dinner ]
  config.update.columns = [:course_date, :all_day, :venue, :morning1, :filemorning1, :morning2, :filemorning2,:morning3, :filemorning3, :lunch,:afternoon1,:afternoon1b,:afternoon2,:dinner ]
  config.show.columns = [:course_date, :all_day, :venue, :morning1, :filemorning1, :morning2, :filemorning2,:morning3, :filemorning3, :lunch,:afternoon1,:afternoon1b,:afternoon2,:dinner ]

  #config.columns[:filemorning1].label = "Speaker's File"
  #config.columns[:filemorning2].label = "Speaker's File"
  #config.columns[:filemorning3].label = "Speaker's File"



  end

  def conditions_for_collection
    ["course_year = ?", "#{APP_CONFIG['course_year']}"]
    end
  
end
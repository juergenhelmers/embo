class WelcomeController < ApplicationController

  layout 'course'

  def index 
    @schedule = Schedule.find(:all, :conditions => ["course_date=?", Time.now])[0]  || []
  end
end

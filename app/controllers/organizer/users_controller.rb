class Organizer::UsersController < Organizer::OrganizerController
  active_scaffold :user do |config|
    
    
    config.actions.exclude :create
    config.actions.exclude :update
    config.actions.exclude :delete
       
    #labels
    config.list.label = "System Users"
    config.show.label = "User Details"
    config.columns[:login].label = "User Name"
    config.columns[:first_name].label = "First Name"
    config.columns[:last_name].label = "Last Name"
    config.columns[:email].label = "Email Address"
    config.columns[:active].label = "Accepted"
    config.columns[:datafiles].label = "Letter(s) of Recommendation"
    
    
      
    
    #sorting
    config.list.sorting = { :id => :ASC }
    
    #column definitions
    config.columns = [:login, :admin, :first_name, :last_name, :email]
    config.list.columns = [:id, :login, :first_name, :last_name, :email, :active]
    config.show.columns = [:course_year, :id, :login, :active, :title, :first_name, :last_name, :email,
    :birthday, :nationality, :department, :organisation, :address1, :address2, :postal_code, :city,
    :user_country, :telephone, :fax_number, :mobile,:current_position, :supervisor, 
    :learn_course, :reason_to_apply, :mic_exp, :languages, :science_exp, :awards, :publications,
    :background, :benefit, :cv_file, :datafiles]


end

  def conditions_for_collection
    "course_year = #{APP_CONFIG['course_year'].to_s} AND role_id='3'"
  end



  def rate
    @record = User.find(params[:id])
    @record.rate(params[:stars], current_user, params[:dimension])
    render :update do |page|
      page.replace_html @record.wrapper_dom_id(params), ratings_for(@record, params.merge(:wrap => false))
      page.visual_effect :highlight, @record.wrapper_dom_id(params)
    end
  end

end

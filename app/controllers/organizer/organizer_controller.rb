class Organizer::OrganizerController < ApplicationController
  layout 'admin'
  before_filter :organizer_required
end

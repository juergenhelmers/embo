class ReferalsController < ApplicationController
  layout 'course'
  # GET /referals/new
  # GET /referals/new.xml
  def new
    @referal = Referal.new
  end

  # POST /referals
  # POST /referals.xml
  def create
    @referal = Referal.new(params[:referal])

    respond_to do |format|
      if @referal.errors.empty? 
        if @referal.save 
          flash[:notice] = 'Your colleague has been successfully informed of this course.'
          UserMailer.deliver_referal(@referal)
          format.html { redirect_to welcomes_path }
        else 
          format.html { render :action => "new" }
        end
      else
        format.html { render :action => "new" }
        
      end
    end
  end

  end

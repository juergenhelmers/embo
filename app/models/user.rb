require 'digest/sha1'

class User < ActiveRecord::Base
  ajaxful_rateable :stars => APP_CONFIG['max_rating_stars'], :allow_update => true

  ajaxful_rater

  def to_label
    first_name + ' ' + last_name
  end

  def full_name
    first_name + ' ' + last_name
  end

  def display_name
    first_name
  end


  #callbacks
  before_create :set_registration_defaults
  
  has_many :datafiles
  

  
  
  #paperclip attachment of CV file in pdf format, see validations
  has_attached_file :cv_file,
                  :url  => "/system/assets/users/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/system/assets/users/:id/:style/:basename.:extension"

  #validation of extra course fields
  validates_attachment_presence :cv_file,   :on => :create
  validates_attachment_size :cv_file, :on => :create, :less_than => 5.megabytes
#  validates_attachment_content_type :cv_file,:on => :create, :content_type => ['application/pdf']
  validates_presence_of     :first_name,                 :on => :create
  validates_presence_of     :last_name,                  :on => :create
  validates_presence_of     :address1,                   :on => :create 
  validates_presence_of     :postal_code,                :on => :create
  validates_presence_of     :city,                       :on => :create 
  validates_presence_of     :user_country,               :on => :create 
  validates_presence_of     :organisation,               :on => :create 
  validates_presence_of     :title,                      :on => :create 
  validates_presence_of     :telephone,                  :on => :create 
  validates_presence_of     :birthday,                   :on => :create 
  validates_presence_of     :nationality,                :on => :create 
  validates_presence_of     :fax_number,                 :on => :create
  validates_presence_of     :current_position,           :on => :create
  validates_presence_of     :supervisor,                 :on => :create
  validates_presence_of     :hear_about_the_course,      :on => :create 
  validates_presence_of     :reason_to_apply,            :on => :create 
  validates_presence_of     :mic_exp,                    :on => :create 
  validates_presence_of     :languages,                  :on => :create
  validates_presence_of     :science_exp,                :on => :create
  validates_presence_of     :background,                 :on => :create
  validates_presence_of     :benefit,                    :on => :create
  validates_length_of       :current_position,           :maximum => 200
  validates_length_of       :supervisor,                 :maximum => 200
  validates_length_of       :languages,                  :maximum => 200
  validates_length_of       :awards,                     :maximum => 200

  # named scopes
  named_scope :accepted_students, :conditions => ['users.role_id = ? AND users.accepted = ? AND users.course_year = ?', 3, true, APP_CONFIG['course_year'].to_s ]
  named_scope :active, :conditions => ['users.active = ?', true]
  
  # model based AS security calls for user model functions
  def authorized_for_accept_user?
    return true if self.active == false && self.accepted == nil
  end
  
  def authorized_for_reject_user?
    return true if self.active == false && self.accepted == nil
  end
  
   ## Instance Methods
   
  def deactivate
    return if admin? #don't allow admin deactivation
    @activated = false
    update_attributes(:activated_at => nil, :active => false, :activation_code => make_activation_code)
  end
  
  def activate
    @activated = true
    update_attributes(:activated_at => Time.now.utc, :activation_code => nil, :active => true)
  end
  
  def active?
    self.active == true
  end
  
  # callback functions
  def set_registration_defaults
    self.role_id = 3
    self.course_year = APP_CONFIG['course_year'].to_s
    self.activated_at = ''
    self.active = 0
  end
  
  # override existing functions in CE user.rb
  def deliver_activation
    #UserNotifier.deliver_activation(self) if self.recently_activated?
    return true
  end
  
  def deliver_signup_notification
    #UserNotifier.deliver_signup_notification(self)
    return true    
  end

  
  protected

    def make_activation_code
     # self.activation_code = Digest::SHA1.hexdigest( Time.now.to_s.split(//).sort_by {rand}.join )
     return true
    end
  
end

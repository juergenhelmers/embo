class Binary < ActiveRecord::Base
  
   belongs_to :user
   before_create :assign_course_year
   
   # named scopes
   named_scope :published, {:conditions => ['binaries.published = ? and binaries.document_course_year = ?', true, APP_CONFIG['course_year'].to_s], :order => 'created_at desc' }
   
  
   has_attached_file :document, :styles => {:thumbnail => ["48x48>", :png] },
    :url  => "/system/assets/documents/:id/:style/:basename.:extension",
    :path => ":rails_root/public/system/assets/documents/:id/:style/:basename.:extension"
    before_post_process :image?
  validates_attachment_presence :document
  validates_attachment_size :document, :less_than => 50.megabytes
  #validates_attachment_content_type :document, :content_type => ['application/pdf', 'application/msword', 'application/vnd.oasis.opendocument.text', 'application/rtf', 'text/plain' ]

   
   def to_label
    document_file_name
  end
  
  def assign_course_year
    self.document_course_year = APP_CONFIG['course_year']
  end

  private 

  def image?
    !(document_content_type =~ /^image.*/).nil?
  end
   
end

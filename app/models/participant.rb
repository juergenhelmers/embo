require 'digest/sha1'

class Participant < ActiveRecord::Base
  
  
  def self.table_name() "users" end

  default_scope  :conditions => { :role_id => '5' }
  
  def to_label
    first_name + ' ' + last_name
  end

  #callbacks
  before_create :set_registration_defaults
  before_validation_on_create :generate_password
  before_save   :generate_login_slug
  
  acts_as_authentic do |c|
    c.crypto_provider = CommunityEngineSha1CryptoMethod

    c.validates_length_of_password_field_options = { :within => 6..20, :if => :password_required? }
    c.validates_length_of_password_confirmation_field_options = { :within => 6..20, :if => :password_required? }

    c.validates_length_of_login_field_options = { :within => 5..20 }
    c.validates_format_of_login_field_options = { :with => /^[\sA-Za-z0-9_-]+$/ }

    c.validates_length_of_email_field_options = { :within => 3..100 }
    c.validates_format_of_email_field_options = { :with => /^([^@\s]+)@((?:[-a-z0-9A-Z]+\.)+[a-zA-Z]{2,})$/ }
  end
  
  
  #validation of extra course fields
  validates_presence_of     :first_name,                 :on => :create
  validates_presence_of     :last_name,                  :on => :create
  validates_presence_of     :email,                      :on => :create
  validates_presence_of     :login, :email
  validates_presence_of     :password,                   :if => :password_required?
  validates_presence_of     :password_confirmation,      :if => :password_required?
  validates_length_of       :password, :within => 6..20, :if => :password_required?
  validates_confirmation_of  :password,                   :if => :password_required?
  validates_length_of       :login,    :within => 5..20
  validates_length_of       :email,    :within => 3..100
  validates_format_of       :email, :with => /^([^@\s]+)@((?:[-a-z0-9A-Z]+\.)+[a-zA-Z]{2,})$/
  validates_format_of       :login, :with => /^[\sA-Za-z0-9_-]+$/
  validates_uniqueness_of    :login, :email, :case_sensitive => false
  validates_uniqueness_of    :login_slug
  validates_exclusion_of     :login, :in => AppConfig.reserved_logins
  validates_date            :birthday, :before => 18.years.ago.to_date  
   ## Instance Methods
   
  def deactivate
    return if admin? #don't allow admin deactivation
    @activated = false
    update_attributes(:activated_at => nil, :active => false, :activation_code => make_activation_code)
  end
  
  def activate
    @activated = true
    update_attributes(:activated_at => Time.now.utc, :activation_code => nil, :active => true)
  end
  
  def active?
    activation_code.nil?
  end
  
  
  
  
  
  # callback functions
  def set_registration_defaults
    self.role_id = 5
    self.course_year = APP_CONFIG['course_year']
    self.activated_at = Time.now.utc
    self.active = 1
  end
  
  # before filter
  def generate_login_slug
    self.login_slug = self.login.parameterize
  end
  
  private 

  def generate_password
    self.password = Array.new(12) { (rand(122-97) + 97).chr }.join
    self.password_confirmation = self.password
  end
  
  protected
  def password_required?
    crypted_password.blank? || !password.blank?
  end

end

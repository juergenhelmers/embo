class Datafile < ActiveRecord::Base
  
  belongs_to :user
  
  has_attached_file :letter,  
    :url  => "/system/assets/letters/:id/:style/:basename.:extension",
    :path => ":rails_root/public/system/assets/letters/:id/:style/:basename.:extension"
    
  validates_attachment_presence :letter
  validates_attachment_size :letter, :less_than => 5.megabytes
  #validates_attachment_content_type :letter, :content_type => ['application/pdf', 'application/msword', 'application/vnd.oasis.opendocument.text', 'application/rtf', 'text/plain' ]

  def to_label
    letter_file_name
  end

end

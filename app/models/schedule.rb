class Schedule < ActiveRecord::Base

  before_save :set_course_year

  named_scope :today, :conditions => { :course_date => Time.now.strftime('%Y-%m-%d').to_s }
  named_scope :sorted, lambda { |s| { :order => 'course_date ASC' }  }


  has_attached_file :filemorning1, :url => "/system/assets/schedules/:id/:style/:basename.:extension",
  :path => ":rails_root/public/assets/system/schedules/:id/:style/:basename.:extension"
  #validates_attachment_presence :filemorning1
  #validates_attachment_size :filemorning1, :less_than => 5.megabytes
  #validates_attachment_content_type :filemorning1, :content_type => ['application/pdf']
  
  has_attached_file :filemorning2, :url => "/system/assets/schedules/:id/:style/:basename.:extension",
  :path => ":rails_root/public/system/assets/schedules/:id/:style/:basename.:extension"
  #validates_attachment_presence :filemorning2
  #validates_attachment_size :filemorning2, :less_than => 5.megabytes
  #validates_attachment_content_type :filemorning2, :content_type => ['application/pdf']
  
  has_attached_file :filemorning3, :url => "/system/assets/schedules/:id/:style/:basename.:extension",
  :path => ":rails_root/public/system/assets/schedules/:id/:style/:basename.:extension"
  #validates_attachment_presence :filemorning3
  #validates_attachment_size :filemorning3, :less_than => 5.megabytes
  #validates_attachment_content_type :filemorning3, :content_type => ['application/pdf']

  private

  def set_course_year
    self.course_year = APP_CONFIG['course_year']
  end

end

class UserNotifier < ActionMailer::Base
    
  def setup_sender_info
    @from       = "microscopy.synbio@gmail.com" 
    @content_type = "text/html"           
  end
  
end

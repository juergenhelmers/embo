class Instructor < ActiveRecord::Base
  
  #has_and_belongs_to_many :programs 
  
  validates_presence_of :first_name, :last_name, :email, :organisation
  
  end

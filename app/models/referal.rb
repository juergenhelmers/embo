class Referal < ActiveRecord::Base
  validates_presence_of     :you_name, :you_email, :name, :email
  validates_length_of       :email,         :within => 3..100
  validates_length_of       :you_email,     :within => 3..100
  validates_format_of       :email, :with => /^([^@\s]+)@((?:[-a-z0-9A-Z]+\.)+[a-zA-Z]{2,})$/
  validates_uniqueness_of   :email, :case_sensitive => false, :message => "We already have sent a referal to this email address."
end

class UserMailer < ActionMailer::Base

  def registration_confirmation(user)

    recipients	user.email
    from	"#{APP_CONFIG['return_email_address']}"
    subject	"Confirmation of Registration: #{APP_CONFIG['title_long']} #{APP_CONFIG['subtitle']}"
    body	:user => user
  end  
  
  def registration_confirmation_ext(user)

    recipients  user.email
    from	"#{APP_CONFIG['return_email_address']}"
    subject 'Confirmation of Registration: "µBOOK - Social Network"'
    body  :user => user
  end 
  
  
  def acceptance_letter(user)
    recipients  user.email
    from	"#{APP_CONFIG['return_email_address']}"
    subject "Confirmation of Acceptance: #{APP_CONFIG['title_long']} #{APP_CONFIG['subtitle']}"
    body  :user => user
  end 

  def rejection_letter(user)
    recipients  user.email
    from	"#{APP_CONFIG['return_email_address']}"
    subject "#{APP_CONFIG['title_long']} #{APP_CONFIG['subtitle']} "
    body  :user => user
  end 
    
  
  def referal(referal)

    recipients  referal.email
    from	"#{APP_CONFIG['return_email_address']}"
    subject "Course Recommendation: #{APP_CONFIG['title_long']} #{APP_CONFIG['subtitle']} "
    body  :referal => referal
  end  

end

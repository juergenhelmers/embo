class PracticalModule < ActiveRecord::Base
  before_save :set_course_year

  private

  def set_course_year
    self.course_year = APP_CONFIG['course_year']
  end
  
end

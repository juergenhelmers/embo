class Speaker < ActiveRecord::Base
  
  validates_presence_of :first_name, :last_name, :organisation, :country, :city
  validates_uniqueness_of :email
  
 # :title, :body, :address, :postal_code, :city, :webpage, :email, :telephone


  def full_name
    self.first_name + " " + self.last_name
  end

end

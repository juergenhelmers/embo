module Organizer::UsersHelper
  def email_column(record)
    mail_to(h(record.email))
  end
  
  def cv_file_column(record)
    link_to record.cv_file_file_name, record.cv_file.url
  end
  
  def datafiles_column(record)
    record.datafiles.collect{|u| link_to("#{u.letter_file_name}", u.letter.url)}.join(' | ')
  end
      
  def active_column(record)
    if record.active
      image_tag "accept.png"
    else
      image_tag "delete.png"
    end
  end

end
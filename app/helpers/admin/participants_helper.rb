module Admin::ParticipantsHelper
  
  def active_column(record)
    if record.active
      image_tag "accept.png"
    elsif record.accepted == nil 
      image_tag "delete_gray.png"
    elsif record.accepted == false
      image_tag "delete.png"
    end
  end
  
  def email_column(record)
    mail_to(h(record.email))
  end

end

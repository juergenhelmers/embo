module Admin::SchedulesHelper
  
  def lunch_form_column(record, _)
    text_field :record, :lunch, {:value => record.lunch || 'Lunch', :class => 'text-input' }
  end
  
   def dinner_form_column(record, _)
    text_field :record, :dinner, {:value => record.dinner || 'Dinner', :class => 'text-input' }
  end
  
  def afternoon1_form_column(record, _)
    text_field :record, :afternoon1, {:value => record.afternoon1 || 'Practical Module', :class => 'text-input' }  
  end
  
  def course_date_form_column(record, _)
    date_select :record, :course_date, { :value => record.course_date || Schedule.last.course_date+1 }
  end
  
  def filemorning1_form_column(record, input_name)
    file_field :record, :filemorning1
  end
  
  def filemorning2_form_column(record, input_name)
    file_field :record, :filemorning2
  end
  
  def filemorning3_form_column(record, input_name)
    file_field :record, :filemorning3
  end
  
end

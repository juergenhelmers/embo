module Admin::AlumnisHelper
  def email_column(record)
    mail_to(h(record.email))
  end
  
  def id_form_column(record, input_name)
    label :record, :id, record.id
  end
  
  def cv_file_form_column(record, options)
    # with date_select we can use :name
    file_field :record, :cv_file, options
    # but if we used select_date we would have to use :prefix
    #select_date record[:date_received], options.merge(:prefix => options[:name])
  end  
    
  def role_id_form_column(record, input_name)
     collection_select(:record, :role_id, Role.all, :id, :name, {:prompt => true})
  end  

  def password_form_column(record, options)
    password_field :record, :password, options
  end
  def password_confirmation_form_column(record, options)
    password_field :record, :password, options
  end

  def letter_of_recommendation_text_show_column(record)
    sanitize(record.letter_of_recommendation_text)
  end
  
  def letter_of_recommendation_text2_show_column(record)
    sanitize(record.letter_of_recommendation_text2)
  end
  
  def created_at_show_column(record)
    deadline = '2010-04-02'
    deadline = deadline.to_date
    if record.created_at <= deadline
      record.created_at.strftime('<span class="green">%Y-%m-%d (at %H:%M)</span>')
    else
      record.created_at.strftime('<span class="red"><strong>%Y-%m-%d (at %H:%M) Applicant missed the Deadline.</strong></span>')
    end
  
  end
  
  def cv_file_column(record)
    link_to record.cv_file_file_name, record.cv_file.url
  end
  
  def datafiles_column(record)
    record.datafiles.collect{|u| link_to("#{u.letter_file_name}", u.letter.url)}.join(' | ')
  end
      
  def active_column(record)
    if record.active
      image_tag "accept.png"
    elsif record.accepted == nil 
      image_tag "delete_gray.png"
    elsif record.accepted == false
      image_tag "delete.png"
    end
  end

end

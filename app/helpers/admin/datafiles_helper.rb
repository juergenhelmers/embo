module Admin::DatafilesHelper
# def letter_form_column(record,input_name)
#   file_field_tag input_name, :size => '40'
# end
 
 def description_form_column(record,input_name)
   text_area :record, :description, :name => input_name, :rows => '3'
 end
 
 def letter_column(record)
    
    # initialize the icon path with a default icon in case something goes wrong
    icon = '/images/mime-types/unknown.png'

    if /^text\//.match(record.letter_content_type)
       icon = '/images/mime-types/text-plain.png'
    elsif
      /^image\//.match(record.letter_content_type)
       icon = '/images/mime-types/image-x-generic.png'
    elsif
       /\/pdf/.match(record.letter_content_type)
       icon = '/images/mime-types/application-pdf.png'
    elsif
       /\/x-rar/.match(record.letter_content_type)
       icon = '/images/mime-types/application-x-rar.png'
    elsif
       /\/vnd.oasis.opendocument.spreadsheet/.match(record.letter_content_type)
       icon = '/images/mime-types/application-vnd.oasis.opendocument.spreadsheet.png'
    elsif
       /\/vnd.oasis.opendocument.text/.match(record.letter_content_type)
       icon = '/images/mime-types/application-vnd.oasis.opendocument.text.png'
    elsif
       /\/vnd.oasis.opendocument.presentation/.match(record.letter_content_type)
       icon = '/images/mime-types/application-vnd.oasis.opendocument.presentation.png'
    elsif
       /\/msword/.match(record.letter_content_type)
       icon = '/images/mime-types/application-msword.png'
    end
    # add the link to the attachement file using the icon and the filename
    link_to(image_tag(icon) + " " + record.letter_file_name, record.letter.url )
  
  end
 
end

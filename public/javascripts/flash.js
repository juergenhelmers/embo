jQuery(document).ready(function(){
	jQuery.noConflict()
  setTimeout(hideFlashes, 6000);
});

var hideFlashes = function() {
  jQuery('.flash_message').fadeOut(2000);
}
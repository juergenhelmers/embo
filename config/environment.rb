# Be sure to restart your server when you modify this file

# Specifies gem version of Rails to use when vendor/rails is not present
RAILS_GEM_VERSION = '2.3.17' unless defined? RAILS_GEM_VERSION

# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')
 require 'desert'

Rails::Initializer.run do |config|
  config.plugins = [:community_engine, :white_list, :all]
config.plugin_paths += ["#{RAILS_ROOT}/vendor/plugins/community_engine/plugins"]

  config.gem 'icalendar', :version => '1.1.5'
  config.gem 'calendar_date_select', :version => '1.16.2'
  config.gem 'aws-s3', :lib => 'aws/s3'
  config.gem 'haml', :lib => 'htmlentities', :version => '3.1.2'
  config.gem 'htmlentities', :lib => 'htmlentities'
  config.gem 'hpricot', :lib => 'hpricot', :version => '0.8.4'
  config.gem 'rmagick', :lib => 'RMagick'
  config.gem 'desert', :version => '0.5.3', :lib => 'desert'
  #config.gem 'kete-tiny_mce', :lib => 'tiny_mce', :source => 'http://gems.github.com'
  config.gem 'authlogic', :version => '2.1.6'
  config.gem 'ri_cal', :version => '0.8.8'
  config.gem 'searchlogic', :version => '2.5.3'
  config.gem "ajaxful_rating"
  config.gem "rack", :version => '1.1.0'
  config.gem 'rdoc'


#add .l method to extension that CE adds onto the symbol class.
#require_dependency 'vendor/plugins/community_engine/lib/globalite_extensions.rb'


  
  # Settings in config/environments/* take precedence over those specified here.
  # Application configuration should go into files in config/initializers
  # -- all .rb files in that directory are automatically loaded.

  # Add additional load paths for your own custom dirs
  # config.load_paths += %W( #{RAILS_ROOT}/extras )

  # Specify gems that this application depends on and have them installed with rake gems:install
  # config.gem "bj"
  # config.gem "hpricot", :version => '0.6', :source => "http://code.whytheluckystiff.net"
  # config.gem "sqlite3-ruby", :lib => "sqlite3"
  # config.gem "aws-s3", :lib => "aws/s3"

  # Only load the plugins named here, in the order given (default is alphabetical).
  # :all can be used as a placeholder for all plugins not explicitly named
  # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

  # Skip frameworks you're not going to use. To use Rails without a database,
  # you must remove the Active Record framework.
  # config.frameworks -= [ :active_record, :active_resource, :action_mailer ]

  # Activate observers that should always be running
  # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

  # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
  # Run "rake -D time" for a list of tasks for finding time zone names.
  config.time_zone = 'UTC'

  # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
  # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}')]
  # config.i18n.default_locale = :de
end
 require "#{RAILS_ROOT}/vendor/plugins/community_engine/config/boot.rb"

  CalendarDateSelect.format = :iso_date

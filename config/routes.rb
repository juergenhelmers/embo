ActionController::Routing::Routes.draw do |map|
  map.resources :practical_modules

  map.resources :schedules
  
  map.resources :groups

  map.resources :last_course
  
  map.resources :referals

  map.resources :speakers

  map.resources :instructors

  map.resources :welcome

  map.resources :flyers

  map.resources :venues

  map.resources :programs

  map.resources :referals

  map.resources :course_signups

  map.resources :documents, :controller => :binaries
  
  map.signup '/signup', :controller => 'course_signups', :action => 'index'
  map.home '', :controller => 'welcome', :action => 'index'
  
   map.namespace :admin do |admin|
       admin.resources :users, :member => {:rate => :post}, :active_scaffold => true
       admin.resources :alumnis, :active_scaffold => true
       admin.resources :instructors, :active_scaffold => true
       admin.resources :speakers, :active_scaffold => true
       admin.resources :referals, :active_scaffold => true
       admin.resources :participants, :active_scaffold => true
       admin.resources :datafiles, :active_scaffold => true
       admin.resources :schedules, :active_scaffold => true
       admin.resources :practical_modules, :active_scaffold => true
       admin.resources :binaries, :active_scaffold => true
       map.resources :collections, :collection=>{:select=>:post}, :active_scaffold => true do |collection|
          collection.resources :users, :member=>{:accept_user=>:put}, :active_scaffold => true
          collection.resources :users, :member=>{:reject_user=>:put}, :active_scaffold => true
          collection.resources :users, :member=>{:rate=>:get}, :active_scaffold => true 
      end
  end
  
  map.namespace :organizer do |organizer|
       organizer.resources :users, :member => {:rate => :post}, :active_scaffold => true
  end
 
  # See how all your routes lay out with "rake routes"
  map.routes_from_plugin :community_engine
  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing or commenting them out if you're using named routes and resources.
  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
end

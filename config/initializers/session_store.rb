# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_embo_session',
  :secret      => '59ac08df265f8979e4b1360bb826648844dcb9063bdc1b0682ea80f775faaaf9e1bf9583b00e0bdc06d2471efd676c291eb2d472f8efbd88cf8bdcd4357d3561'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store

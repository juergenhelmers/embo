# server details
set :application, "embo"                                                  # set application name
set :deploy_to, "/backup/www-rails/capistrano/#{application}"   # define target directory
default_run_options[:pty] = true
ssh_options[:forward_agent] = true                          # enable ssh forwarding
set :deploy_via, :remote_cache                                   # use rsync to only copy diffs
set :keep_releases, 4                                                         # keep only 4 releases at a time

set :user, "helmerj"                                                            # deploy as this user
set :use_sudo, true                                                              # don't use sudo
role :web, "46.38.235.18"                                               # HTTP server, nginx
role :app, "46.38.235.18"                                                # application server
role :db,  "46.38.235.18", :primary => true            # database server

# repo details
set :scm, :git                                                                          # specify the repository type
set :scm_username, "helmerj"                                   # specify the username to access the repository
set :repository, "keiblinger.com:/backup/git/embo"              # specify the repository
set :branch, "master"                                                       # specify the repository branch

# setup shared folder for persitant files
set :shared_assets, %w{public/system }

namespace :assets  do
  namespace :symlinks do
    desc "Setup application symlinks for shared assets"
    task :setup, :roles => [:app, :web] do
      shared_assets.each { |link| run "mkdir -p #{shared_path}/#{link}" }
    end

    desc "Link assets for current deploy to the shared location"
    task :update, :roles => [:app, :web] do
      shared_assets.each { |link| run "ln -nfs #{shared_path}/#{link} #{release_path}/#{link}" }
    end
  end
end
before "deploy:setup" do
  assets.symlinks.setup
end

before "deploy:symlink" do
  assets.symlinks.update
end
# hooks
before "deploy:setup", "assets:symlinks:setup"                    # created required directories
before "deploy:symlink", "assets:symlinks:update"            # udpate persistent directory links
before "deploy:migrate", "deploy:create_configuration_files"
after "deploy:update_code", "deploy:migrate"         # migrate database
after "deploy:restart", "deploy:cleanup"                                  # cleanup after restart to limit to max number of releases

 namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :create_configuration_files, :roles => :app do
     run "ln -fs #{File.join(release_path,'config','database.yml.deploy')} #{File.join(release_path,'config','database.yml')}"
     run "ln -fs #{File.join(shared_path,'config','config.yml.deploy')} #{File.join(release_path,'config','config.yml')}"
   end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end
 end
 
task :bundle_gems, :roles => :app do
  run "mkdir -p #{shared_path}/bundle && ln -s #{shared_path}/bundle #{release_path}/vendor/bundle"
  run "cd #{latest_release}; /opt/ruby-enterprise-1.8.7-2009.10/bin/bundle install --deployment --without development test"
end
 
#desc "List used application server libraries"
#task :list_gems, :roles => :app do
#  run "gem list"
#end

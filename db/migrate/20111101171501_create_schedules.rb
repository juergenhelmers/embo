class CreateSchedules < ActiveRecord::Migration
  def self.up
    create_table :schedules do |t|
      t.string :morning1
      t.string :morning2
      t.string :morning3
      t.string :lunch
      t.string :afternoon1
      t.string :afternoon2
      t.string :dinner
      t.boolean :all_day
      t.date :course_date 

      t.timestamps
    end
  end

  def self.down
    drop_table :schedules
  end
end

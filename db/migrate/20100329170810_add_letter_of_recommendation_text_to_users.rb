class AddLetterOfRecommendationTextToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :letter_of_recommendation_text, :text
  end

  def self.down
    remove_column :users, :letter_of_recommendation_text
  end
end

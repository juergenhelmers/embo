class AddDocumentCourseYearToBinaries < ActiveRecord::Migration
  def self.up
    add_column :binaries, :document_course_year, :string
  end

  def self.down
    remove_column :binaries, :document_course_year
  end
end

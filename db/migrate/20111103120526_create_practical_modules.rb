class CreatePracticalModules < ActiveRecord::Migration
  def self.up
    create_table :practical_modules do |t|
      t.date :course_date
      t.string :group1_early
      t.string :group1_late
      t.string :group2_early
      t.string :group2_late
      t.string :group3_early
      t.string :group3_late
      t.string :group4_early
      t.string :group4_late

      t.timestamps
    end
  end

  def self.down
    drop_table :practical_modules
  end
end

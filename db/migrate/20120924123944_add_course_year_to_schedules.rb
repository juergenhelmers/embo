class AddCourseYearToSchedules < ActiveRecord::Migration
  def self.up
    add_column :schedules, :course_year, :string
  end

  def self.down
    remove_column :schedules, :course_year
  end
end

class AddLetterOfRecommendationText2ToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :letter_of_recommendation_text2, :text
  end

  def self.down
    remove_column :users, :letter_of_recommendation_text2
  end
end

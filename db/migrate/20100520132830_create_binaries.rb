class CreateBinaries < ActiveRecord::Migration
  def self.up
    create_table  :binaries do |t|
      t.string    :document_file_name, :document_content_type
      t.text      :comment
      t.integer   :document_file_size, :user_id
      t.datetime  :document_updated_at
      t.boolean   :published
      t.timestamps
    end
  end

  def self.down
    drop_table :binaries
  end
end

class AddCourseYearToPracticalModules < ActiveRecord::Migration
  def self.up
    add_column :practical_modules, :course_year, :string
  end

  def self.down
    remove_column :practical_modules, :course_year
  end
end

class AddMoreFieldsToUser < ActiveRecord::Migration
  def self.up
      add_column :users, :city, :string
      add_column :users, :address1, :string      
      add_column :users, :address2, :string      
      add_column :users, :postal_code, :string        
      add_column :users, :user_country, :string      
      add_column :users, :department, :string      
      add_column :users, :organisation, :string      
      add_column :users, :title, :string      
      add_column :users, :telephone, :string      
      add_column :users, :date_of_birth, :date      
      add_column :users, :letter, :string
      add_column :users, :nationality, :string
      add_column :users, :fax_number, :string
      add_column :users, :mobile, :string
      add_column :users, :current_position, :string
      add_column :users, :supervisor, :string
      add_column :users, :learn_course, :text
      add_column :users, :reason_to_apply, :text
      add_column :users, :mic_exp, :text
      add_column :users, :languages, :string
      add_column :users, :science_exp, :text
      add_column :users, :awards, :text
      add_column :users, :publications, :text
      add_column :users, :background, :text
      add_column :users, :benefit, :text
      add_column :users, :race, :string
      add_column :users, :course_year, :string
  end

  def self.down
    remove_column :users, :city
  end
end

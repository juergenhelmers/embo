class CreateReferals < ActiveRecord::Migration
  def self.up
    create_table :referals do |t|
      t.string :you_name
      t.string :you_email
      t.string :name
      t.string :email
      t.string :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :referals
  end
end

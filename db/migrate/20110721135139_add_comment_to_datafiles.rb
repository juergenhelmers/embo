class AddCommentToDatafiles < ActiveRecord::Migration
  def self.up
    add_column :datafiles, :comment, :text
    remove_column :datafiles, :description
  end

  def self.down
    add_column :datafiles, :description, :text
    remove_column :datafiles, :comment
  end
end

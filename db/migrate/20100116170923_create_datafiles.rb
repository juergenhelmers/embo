class CreateDatafiles < ActiveRecord::Migration
  def self.up
  create_table :datafiles do |t|
    t.string  :filename
    t.integer :user_id
  end 
  end

  def self.down
  drop table :datafiles
  end
end

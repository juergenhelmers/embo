class CreateInstructorProgramJoin < ActiveRecord::Migration
  def self.up
    create_table 'instructors_programs', :id => 'false' do |t|
      t.column 'instructor_id', :integer
      t.column 'program_id', :integer
    end
  end

  def self.down
    drop_table 'instructors_programs'
  end
end

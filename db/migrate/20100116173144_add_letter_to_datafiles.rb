class AddLetterToDatafiles < ActiveRecord::Migration
  def self.up
    add_column :datafiles, :description, :text
    add_column :datafiles, :letter_file_name, :string
    add_column :datafiles, :letter_content_type, :string
    add_column :datafiles, :letter_file_size, :integer
    add_column :datafiles, :letter_updated_at, :datetime
  end

  def self.down
    remove_column :datafiles, :letter_updated_at
    remove_column :datafiles, :letter_file_size
    remove_column :datafiles, :letter_content_type
    remove_column :datafiles, :letter_file_name
  end
end

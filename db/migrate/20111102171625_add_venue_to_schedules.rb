class AddVenueToSchedules < ActiveRecord::Migration
  def self.up
    add_column :schedules, :venue, :string
  end

  def self.down
    remove_column :schedules, :venue
  end
end

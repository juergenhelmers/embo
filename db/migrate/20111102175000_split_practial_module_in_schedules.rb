class SplitPractialModuleInSchedules < ActiveRecord::Migration
  def self.up
    add_column :schedules, :afternoon1b, :string
  end

  def self.down
    remove_column :schedules, :afternoon1b
  end
end

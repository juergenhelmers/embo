class AddExtraRoles < ActiveRecord::Migration
  def self.up
    Role.enumeration_model_updates_permitted = true
    Role.create(:name => 'organizer')
    Role.create(:name => 'participant')
    Role.enumeration_model_updates_permitted = false
  end

  def self.down
  end
end

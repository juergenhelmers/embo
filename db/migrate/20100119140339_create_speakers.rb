class CreateSpeakers < ActiveRecord::Migration
  def self.up
    create_table :speakers do |t|
      t.string :first_name
      t.string :last_name
      t.string :title
      t.text   :body
      t.string :department
      t.string :organisation
      t.string :address
      t.string :postal_code
      t.string :city
      t.string :country
      t.string :telephone
      t.string :email
      t.string :webpage

      t.timestamps

    end
  end

  def self.down
    drop_table :speakers
  end
end

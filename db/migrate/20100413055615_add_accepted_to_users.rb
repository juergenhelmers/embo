class AddAcceptedToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :accepted, :boolean
  end

  def self.down
    remove_column :users, :accepted
  end
end

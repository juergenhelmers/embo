class AddWavedToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :waved, :boolean
    add_column :users, :hear_about_the_course, :text
  end

  def self.down
    remove_column :users, :hear_about_the_course
    remove_column :users, :waved
  end
end
